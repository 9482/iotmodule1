# INDUSTRY 

![industrial revolution](https://www.netobjex.com/wp-content/uploads/2018/12/1-1.png)

Industries play an important role in the economic development of their countries. Due to this industrial revolution is going on continuously and the image describes the 4 main stages of that revolution.

## Industry 1.0 :
  This is the starting stage of industrial revolution where mechanisation and usage of steam power mainly developed in this stage.

## Industry 2.0 :
  Here we get into mass production and efficient utilisation of electrical energy.

## Industry 3.0 :
  This is the stage where the game changer enters to industries and those are computers. And automation of machines had been developed by electronics  and communication became more efficient with the machines used in industries.

![ind 3.0](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/2020-07-28__2_.png)
## Industry 4.0 :
   now what we are seeing the industries are in this stage, where we store the data of all the divisions of industry 3.0 on internet and access it efficiently to maximise the productivity, efficiency and profit for the industries, this process is called as Industrial Internet of Things(IIoT). And also cyber physical systems(CPS) and cloud computing also implemented on manufacturing processes.
This technology's impact include:
* Interoperability
* Decentralization of information
* Real-time data collection
* Heightened flexibility <br/>
As such, the difference between Industry 3.0 and Industry 4.0 is the presence of new interconnected technologies in plant operations.

 ![ind 4.0](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/4.png)

## What is the difference between Industry 3.0 and Industry 4.0?

In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it. Where Industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor.

For an example of the old way (Industry 3.0), take a CNC machine: while largely automated, it still needs input from a human controller. The process is automated based on human input, not by data. Under Industry 4.0, that same CNC machine would not only be able to follow set programming parameters, but also use data to streamline production processes.

![3.o to 4.0](https://qph.fs.quoracdn.net/main-qimg-25782847f73e3000e5dd12704168ad18.webp)

## What will Industry 4.0 look like?

In terms of maintenance, Industry 4.0 means lots of data can be collected through sensors and used to make decisions about repairs and upkeep. Predictive maintenance systems are even beginning to implement machine learning to determine when asset failure may be imminent and prescribe preventive measures.

![Industry 4.0](https://www.wipro.com/content/dam/nexus/en/service-lines/product-engineering/infographics/iot-in-the-manufacturing-industry-enabling-industry-4-0-1.png)

Now, this doesn’t mean we weren’t using data before—we’ve been gathering data for decades. The difference is simply the sheer volume of data available and the new methods we have for handling it all.

IIoT with CPS and cloud-based processes allow us to gather and interpret data in ways that weren’t possible before, and the impact of those technologies is being felt in every aspect of manufacturing, from production to maintenance to marketing, and even then on to the final products we create.

By all this advantage and techinical benefits, Industry 4.0 is the way of the future.

 # INDUSTRY 4.0 COMMUNICATION PROTOCOLS
 *  Mqtt
 *  CoAP
 *  AMQP
 *  HTTP
 *  WebSockets
 *  RESTful API

# PROBLEMS WITH INDUSTRY 4.0 UPGRADES
 *  Cost
 *  Downtime
 *  Reliability

### Roadmap to making our own Industrial IoT

**Step 1:**  Identify most popular Industry 3.0 devices..
**Step 2:** Study protocols that these devices communicate..
**Step 3:** Get data from Industry 3.0 devices..
**Step 4:** Send the data to cloud for Industry 4.0..

#### Examples of IOT Platforms
*  Google IOT
*  AWS IOT 
*  Azure IOT
*  Thingsboard

### Implementation
***
* collecting data from different (or a particular) industrial devices and send it to cloud.
* After sending data to cloud, Data Analysis is carried out with the help of several tools.
* Various tools are: 
  * **IoT TSDB Tools:**  
    * Store the data in Time series databases.
    * Eg: Prometheus, InfluxDB.
  * **IoT Dashboards:**  
    * View all the data into beautiful dashboards.
    * Eg: Grafana, Thingsboard.
  * **IoT Platforms:**  
    * Analyse the data on the platforms like; AWS IoT, Google IoT, Azure IoT, Thingsboard.
  * **Getting Alerts**  
    * Get Alerts based on your data using the platforms like; Zaiper, Twilio.
